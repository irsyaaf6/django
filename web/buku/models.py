from django.db import models

# Create your models here.
class Buku(models.Model):
    code = models.CharField(max_length=50)
    judul = models.CharField(max_length=50)
    penulis = models.CharField(max_length=50)
    penerbit = models.CharField(max_length=50)
    tahun_terbit = models.CharField(max_length=50)
    stok = models.CharField(max_length=50)

    def __str__(self):
        return self.list