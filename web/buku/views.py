from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.
def buku(request):
	tampil = Buku.objects.all()
	template = 'buku.html'
	context = {
		'buku_list' : tampil,
	}
	return render(request, template, context)