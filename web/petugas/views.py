from tempfile import template
from django.shortcuts import render
from .models import *
# Create your views here.
def petugas(request):
	tampil = Petugas.objects.all()
	template = 'petugas.html'
	context = {
		'petugas' : tampil,
	}
	return render(request, template, context)